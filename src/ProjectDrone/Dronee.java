package ProjectDrone;



import javafx.scene.paint.Color;
import java.io.Serializable;



@SuppressWarnings("unused")
public abstract class Dronee implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	double x, y, radian;
	int DroneeID;												
    String colour;								
    static int counter = 0;

    Dronee() {
        this(100, 100, 10);
    }
    /**
     * construct a drone/ball of radian Ballsr at Ballsx,Ballsy of this colour
     * @param Dronex
     * @param Droney
     * @param Droner
     */
    Dronee(double Droneex, double Droneey, double Droneer) {
        x = Droneex;
        y = Droneey;
        radian = Droneer;
        DroneeID = counter++;			
        colour = "Red";
    }
   
    public double getX() { return x; }
   
    public double getY() { return y; }
 
    public double getRad() { return radian; }
 
    public void setXY(double nx, double ny) {
        x = nx;
        y = ny;
    }
  
    public int getID() {return DroneeID; }
  
    public void drawDronee(ItemCanvas mc) {
        mc.drawCircle(x, y, radian, colour);
    }
   
   
    public String toString() {
        return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
    }
    
    public boolean hit(double ox, double oy, double or) {
        return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+radian)*(or+radian);
    }	
    
    protected String getStrType() {
        return "Ball";
    }
   
    protected abstract void adjustDronee();
    /**
     * checks if drone is in arena 
     * @param a
     */
    protected abstract void checkDrone(DroneArena a);
  
   
  	

  
    public boolean hitting (Dronee oDronee) {
        return hit(oDronee.getX(), oDronee.getY(), oDronee.getRad());
    }
	/**
	    * check ball/drone in the arena
	    * @param a BallArena
	    */
	protected void checkDrone1(DroneArena a) {
		// TODO Auto-generated method stub
		
	}
	/**
	    * adjust the drone to a new direction
	    */
	protected void adjustDrone() {
		// TODO Auto-generated method stub
		
	}
}
