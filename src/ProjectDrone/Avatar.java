package ProjectDrone;

import javafx.scene.paint.Color;

/**
 *class to create the Character/paddle object
 */
@SuppressWarnings("unused")
public class Avatar extends Dronee {

    /**Set Character drone/paddle size ir at ix,iy position in the arena and determine colour of object
     * @param ix
     * @param iy
     * @param ir
     */
    public Avatar(double ix, double iy, double ir) {
        super(ix, iy, ir);
        colour = "PURPLE";    
    }
 
    /**
     * draw a rectangle into the canvas at point x,y
     * @param mc
     */
    public void drawDrone(ItemCanvas mc) {
        mc.drawRect(x, y, radian, colour);
    }

    /**
     *  to check drone/ball but has nothing to do
     */
    @Override
    protected void checkDrone(DroneArena a) {
        // nothing to do
    }

    /**
     *  to adjust drone/ball but has nothing to do§1
     */
    @Override
    protected void adjustDronee() {
        // nothing to do
    }
    /**
     *  return string description as Character
     */
    protected String getStrType() {
        return "Avatar";
    }
}
