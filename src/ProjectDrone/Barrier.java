package ProjectDrone;

import javafx.scene.paint.Color;

public class Barrier extends Dronee {
	
	 /** 
     * adjusts obstacle ball/drone has no function
     */
    protected void adjustDronee() {
        // nothing to do
    }
    
    protected String getStrType() {
        return "Barrier";
    }

   
    protected void checkDronee(DroneArena a) {
        // nothing to do
    }

    /**
     * draws the barrier of those attributes in the specified colour
     * @param Barrierx
     * @param Barriery
     * @param Barrierr
     */
    public Barrier(double Barrierx, double Barriery, double Barrierr) {
        super(Barrierx, Barriery, Barrierr);
    }
    
    /**
     * creates and draws obstacles 
     * @param mc
     */
    public void drawDronee(ItemCanvas mc) {
        mc.drawRect(x, y, radian, colour);
    }

	@Override
	protected void checkDrone(DroneArena a) {
		// TODO Auto-generated method stub
		
	}
    
   

    
}
