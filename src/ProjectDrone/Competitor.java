package ProjectDrone;

/**
* class to create the moving blocking drone
*/
public class Competitor extends Dronee {
   private int score;
   private int direction = 1; // Determines left / right direction for target (-1 means left, 1 means right)
   private double speed = 3;
   /**
    * determines direction and speed of the Opponent drone
    */


   /**
    * sets up the score and colour of the created Opponent drone
    * @param ix
    * @param iy
    * @param ir
    */
   public Competitor(double ix, double iy, double ir) {
       super(ix, iy, ir);
       score = 0;
       colour = "RED";
   }
   /**
    * return string defining the object as an Opponent
    */
   protected String getStrType() {
       return "Opponent";
   }

   /**
    * check ball/drone in the arena
    * @param a BallArena
    */
   @Override
   protected void checkDrone1(DroneArena a) {
       if (a.checkHit(this)) {
       	score++;			// if been hit, then increase score
       }
       if (a.inBorder(this) == false) {
       	direction = -direction;
       }
   }
   /**
    * draw Ball and display score
    */
   public void drawDrone(ItemCanvas mc) {
       super.drawDronee(mc);
       mc.showInt(x, y, score);
   }

   /**
    * adjust the drone to a new direction
    */
   @Override
   protected void adjustDrone() {
       x += speed * direction;				
   }
@Override
protected void adjustDronee() {
	// TODO Auto-generated method stub
	
}
@Override
protected void checkDrone(DroneArena a) {
	// TODO Auto-generated method stub
	
}
 
}

