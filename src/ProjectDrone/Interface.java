package ProjectDrone;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Interface extends Application {
    private static final Object DroneArena = null;
	private ItemCanvas mc;
    private AnimationTimer timer;								
    private VBox rtPane;										
    private DroneArena arena;
    
    
    /**
     * set up the mouse event - when mouse pressed move the player object to that position
     * @param canvas
     */
    void MouseClick (Canvas canvas) {
        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, 		// for MOUSE PRESSED event
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        arena.setPaddle(e.getX(), e.getY());
                        drawWorld();							// redraw world
                        drawStatus();
                    }
                });
    }
    /**
     * set up the menu containing the different functions
     * @return the menu bar
     */
    MenuBar setMenu() {
        MenuBar menuBar = new MenuBar();						// create main menu

        Menu mFile = new Menu("FILE");							// add File main menu
        MenuItem mExit = new MenuItem("QUIT");					// whose sub menu has Exit
        mExit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {					// action on exit is
                timer.stop();									// stop timer
                System.exit(0);									// exit program
            }
        });
        //mFile.getItems().addAll(mExit);							// add exit to File menu

        MenuItem mSave = new MenuItem("SAVE");					// whose sub menu has Save
        mSave.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {					// action on exit is
            	save();
            }
        });

        MenuItem mLoad = new MenuItem("LOAD");					// whose sub menu has Save
        mLoad.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {					// action on exit is
                load();
            }
        });
        mFile.getItems().addAll(mExit, mSave, mLoad);							// add exit to File menu

        Menu mHelp = new Menu("HELP");							// create Help menu
        MenuItem mAbout = new MenuItem("ABOUT");				// add About sub men item
        mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                showAbout();									// and its action to print about
            }
        });
        mHelp.getItems().addAll(mAbout);						// add About to Help main item

        menuBar.getMenus().addAll(mFile, mHelp);				// set main menu with File, Help
        return menuBar;											// return the menu
    }

    /**
     * set up the horizontal box for the bottom with relevant buttons
     * @return
     */
    private HBox setButtons() {
        Button btnStart = new Button("START");			// button to start the simulation
        btnStart.setOnAction(click -> timer.start());

        Button btnStop = new Button("STOP");				// button to stop the simulation
        btnStop.setOnAction(click -> timer.stop());

        Button btnAddDrone = new Button("Add Drone");				// button to add another drone/ball
        btnAddDrone.setOnAction(click -> {arena.addDronee(); drawWorld();});

        // now add these buttons + labels to a HBox
        return new HBox(btnStart, btnStop, btnAddDrone);
    }
    
    
    private void showAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);				// define what box is
        alert.setTitle("INFORMATION");									// say is About
        alert.setHeaderText(null);
        alert.setContentText("Welcome to DRONE DASH click to move the drone and knock the balls into the goal without being blocked");			// give text
        alert.showAndWait();										// display the box until the user closes it
    }

    /**
     * draw the score at position x,y
     * @param x
     * @param y
     * @param score
     */
    public void showScore (double x, double y, int score) {
        mc.drawText(x, y, Integer.toString(score));
    }
    /**
     * draw the world with ball in it
     */
    public void drawWorld () {
        mc.clearCanvas();						
        arena.drawDroneArena(mc);
    }

    /**
     *  in pane on right describe the position of each object
     */
    public void drawStatus() {
        rtPane.getChildren().clear();					// clear rtpane
        ArrayList<String> allDs = arena.describeAll();
        for (String s : allDs) {
            Label l = new Label(s); 		// turn description into a label
            rtPane.getChildren().add(l);	// add label
        }
    }


    /**
     *  function to start the program and set up the position of the toolbars and canvas
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("DRONE DASH");
        BorderPane bp = new BorderPane();
        bp.setPadding(new Insets(10, 20, 10, 20));

        bp.setTop(setMenu());											// put at the top

        Group root = new Group();										
        Canvas canvas = new Canvas( 400, 500 );
        root.getChildren().add( canvas );
        bp.setLeft(root);												

        mc = new ItemCanvas(canvas.getGraphicsContext2D(), 400, 500);

        MouseClick(canvas);											

        arena = new DroneArena(400, 500);							
        drawWorld();

        timer = new AnimationTimer() {									
            public void handle(long currentNanoTime) {				
                arena.checkDrones();									
                arena.adjustDrones();								
                drawWorld();									
                drawStatus();										
            }
        };

        rtPane = new VBox();											
        rtPane.setAlignment(Pos.TOP_LEFT);							
        rtPane.setPadding(new Insets(10, 50, 50, 5));					
        bp.setRight(rtPane);											

        bp.setBottom(setButtons());										

        Scene scene = new Scene(bp, 700, 600);							
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());

        primaryStage.setScene(scene);
        primaryStage.show();


    }
    /**
     *  function to load a simulation from the text file
     */
    public void load() {
    	String url = "simulation.txt";
    	try {
    		ObjectInputStream loader = new ObjectInputStream(new FileInputStream(url));
    		loader.close();
    		drawWorld();										// redraw the world
    	} catch(Exception e) {
    		System.out.println(e);
    	}
    }
    /**
     *  function to save the simulation as a string to a text file
     */
    public void save() {
    	String url = "simulation.txt";
    	try {
    		ObjectOutputStream saver = new ObjectOutputStream(new FileOutputStream(url));
    		saver.writeObject(DroneArena);
    		saver.close();
    	} catch(Exception e) {
    		System.out.println(e);
    	}
    }

    /**
     * launches the GUI
     * @param args
     */
    public static void main(String[] args) {
        Application.launch(args);			// launch the GUI

    }
}
