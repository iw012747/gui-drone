package ProjectDrone;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * public c;lass to create arena for drones 
 */
public class DroneArena implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	double ArenaSizeX;
    double ArenaSizeY;						
    private ArrayList<Dronee> allDronees; 
    
    
    /**
     * create Dronearena and add objects 
     * @param xS
     * @param Sizey
     */
    DroneArena(double Sizex, double Sizey){
        ArenaSizeX = Sizex;
        ArenaSizeY = Sizey;
        allDronees = new ArrayList<Dronee>();					//intialize arraylist to be empty 
        allDronees.add(new Ball(Sizex/2, Sizey*0.8, 10, 45, 6));	// adds small black ball
        allDronees.add(new Competitor(Sizex/2, Sizey/5, 35));			// adds enemy drone
        allDronees.add(new Avatar(Sizex/2, Sizey-20, 20));		// adds Character drone
        allDronees.add(new Competitor(Sizex/20, 1*Sizey/5, 25)); // adds blocking objects/walls
        allDronees.add(new Competitor(Sizex/8, 1*Sizey/5, 25));
        allDronees.add(new Competitor(Sizex/4, 1*Sizey/5, 25));
        allDronees.add(new Competitor(Sizex/3, 1*Sizey/5, 25));
        allDronees.add(new Competitor(Sizex, 1*Sizey/5, 25));
        allDronees.add(new Competitor(2*Sizex/3, 1*Sizey/5, 25));
        allDronees.add(new Competitor(2*Sizex/8, 1*Sizey/5, 25));
        allDronees.add(new Competitor(Sizex-100, 1*Sizey/5, 25));
        allDronees.add(new Competitor(Sizex-70, 1*Sizey/5, 25));
        allDronees.add(new Competitor(Sizex-40, 1*Sizey/5, 25));
    }
    
    // sets size of arena 
    DroneArena() {
        this(500, 400);			
    }
    
    //Getter and Setter Functions 
    public double getX() {
        return ArenaSizeX;
    }
    
    public double getY() {
        return ArenaSizeY;
    }
    
   
    /**
     * check all balls/drones, change angle of moving balls if needed
     */
    public void checkDrones() {
        for (Dronee b : allDronees) b.checkDrone(this);	
    }
    
    public void adjustDrones() {
        for (Dronee b : allDronees) b.adjustDrone();
    }
    
    public void drawArena(ItemCanvas mc) {
        for (Dronee b : allDronees) b.drawDronee(mc);	//Function to draw arena parts 
    }
    
   
    /**
     * checks the angle of the drone and its hit conditions depending on a wall or other drone/ball
     * @param x				ball x position
     * @param y				y
     * @param rad			radius
     * @param ang			current angle
     * @param notID			don't check id of the drone/ball
     * @return				new angle
     */
    public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
        double ans = ang;
        if (x < rad || x > ArenaSizeX - rad) ans = 180 - ans;

        if (y < rad || y > ArenaSizeY - rad) ans = - ans;


        for (Dronee b : allDronees)
            if (b.getID() != notID && b.hit(x, y, rad)) ans = 180*Math.atan2(y-b.getY(), x-b.getX())/Math.PI;

        return ans;	
    }
    
    /**
     * creates and sets x and y  coordinates for Avatar 
     * @param x
     * @param y
     */
    public void setPaddle(double x, double y) {
        for (Dronee b : allDronees)
            if (b instanceof Avatar) b.setXY(x, y);
    }

    /**
     * function to check if Avatar has hit a drone 
     * @param Competitor drone 
     * @return 	if hit or not 
     */
    public boolean checkHit(Dronee target) {
        boolean ans = false;
        
		for (Dronee b : allDronees)
            if (b instanceof Ball && b.hitting(target)) ans = true;
        // try all balls
        return ans;
    }
    
    
    /**
     * return list of strings defining each ball/dronee
     * @return
     */
    public ArrayList<String> describeAll() {
        ArrayList<String> ans = new ArrayList<String>();		// set up empty arraylist
        for (Dronee b : allDronees) ans.add(b.toString());			// add string defining each ball
        return ans;												// return string list
    }
    public boolean inBorder(Dronee target) {
    	return !(target.getX() - target.getRad() < 0 || target.getY() - target.getRad() < 0 || target.getX() + target.getRad() > ArenaSizeX || target.getY() + target.getRad() > ArenaSizeY);
    }
    /**
     *adds dronee to certain coordinates 
     * 
     */
    public void addDronee() {
        allDronees.add(new Ball(ArenaSizeX/2, ArenaSizeY*0.8, 10, 60, 5)); }

	public void drawDroneArena(ItemCanvas mc) {
		// TODO Auto-generated method stub
		
	}
   
      }
