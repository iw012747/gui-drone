package ProjectDrone;

/**
 * class to create drone/ball objects
 */
public class Ball extends Dronee {

    double Degree, Speed;			// Degree and speed of travel
    
    
    public Ball(double Ballx, double Bally, double Ballr, double Balla, double Balls) {
        super(Ballx, Bally, Ballr);
        Speed = Balls;
        Degree = Balla;
        colour = "PINK";
    }
    
   

    /**
     * change Degree of the drone 
     * @param a   ballArena
     */
    @Override
    protected void checkDrone(DroneArena a) {
        Degree = a.CheckDroneAngle(x, y, radian, Degree, DroneeID);
    }

    /**
     * change drone depending on speed travelling 
     */
    @Override
    protected void adjustDronee() {
        double radDegree = Degree*Math.PI/180;		
        x += Speed * Math.cos(radDegree);		
        y += Speed * Math.sin(radDegree);		
    }
    /**
     * return and define drone 
     */
    protected String getStrType() {
        return "Drone";
    }

    
    
    /**
     * draw a circle onto the canvas at point x,y
     * @param mc
     */
    public void drawDronee(ItemCanvas mc) {
        mc.drawCircle(x, y, radian, colour);
    }
}

